/**
 * 
 */
package edu.hawaii.ics211;

/**
 * @author Brandon Kondo
 * TestCalculator, a method to test the Calculator class' methods
 */
public class TestCalculator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(Calculator.add(3,4));
		System.out.println(Calculator.add(-1, -8));
		System.out.println(Calculator.subtract(-1, 5));
		System.out.println(Calculator.subtract(9, 5));
		System.out.println(Calculator.add(Calculator.add(2, 3), Calculator.subtract(3, -1)));
		System.out.println(Calculator.divide(1, 2));
		System.out.println(Calculator.divide(3, 1));
		System.out.println(Calculator.multiply(1, 0));
		System.out.println(Calculator.multiply(2, 9));
		System.out.println(Calculator.modulo(10, 9));
		System.out.println(Calculator.modulo(10, 1));
		System.out.println(Calculator.pow(2, 8));
		System.out.println(Calculator.divide(10, 0));
	}

}
